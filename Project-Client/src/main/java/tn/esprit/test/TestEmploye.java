package tn.esprit.test;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.ejb.IEmployerRemote;
import tn.esprit.ejb.IProjetRemote;
import tn.esprit.jpa.Employe;
import tn.esprit.jpa.Nationalite;
import tn.esprit.jpa.Projet;

public class TestEmploye {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		
		InitialContext context = new InitialContext();
		
		IEmployerRemote empservice = (IEmployerRemote) 
					context.lookup("ProjectJPA/EmployeService!tn.esprit.ejb.IEmployerRemote");
		
		IProjetRemote projetRemote = (IProjetRemote) 
			context.lookup("ProjectJPA/ProjetService!tn.esprit.ejb.IProjetRemote");
		
		// add employe 
		
//		Employe e = new Employe();
//		e.setName("Sofien");
//		e.setLastname("Ben Hammadi");
//		e.setNationalite(Nationalite.Tunisienne);
//		
//		empservice.addEmp(e);
//		
//		
//		// test find by id : verifier l'id
//		
//		Employe employe = empservice.findById(1);
//		employe.setName("Mohamed");
//		
//		// update
//		empservice.updateEmp(employe);
//		System.out.println(employe.getName());
//		
//		// delete employe : verifier l'id
//		
//		empservice.deleteEmp(3);
//		
//		// test find ALL
//		
//		List<Employe> employes = empservice.findAllEmploye();
//		
//		System.out.println("///////////// FIND ALL  /////////////");
//		
//		for (Employe e1 : employes) {
//			System.out.println(e1.getName()+" "+e1.getLastname());
//		}
		
		
		// gestion projet
		
		Employe e = new Employe();
		e.setName("Sofien");
		e.setLastname("Ben Hammadi");
		e.setNationalite(Nationalite.Tunisienne);
		
		
		
		Projet p = new Projet();
		p.setLibelle("gestion bib2");
		
		
		Projet p1 = new Projet();
		p1.setLibelle("gestion RH2");
		
		List<Projet> projets = new ArrayList<Projet>();
		projets.add(p);
		projets.add(p1);
		
		e.setProjets(projets);
		
//		projetRemote.addProjet(p);
//		projetRemote.addProjet(p1);
		
		
		empservice.addEmp(e);

		Employe employe = empservice.findById(12);
		List<Projet> lp = employe.getProjets();
		
		for (Projet projet : lp) {
			System.out.println(projet.getLibelle());
		}
	}

}
