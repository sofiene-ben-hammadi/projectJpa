package tn.esprit.beans;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.ejb.IEmployerLocal;
import tn.esprit.jpa.Employe;

@ManagedBean
@SessionScoped
public class LoginBeaan {

	@EJB
	IEmployerLocal employerLocal;
	
	private String name;
	private String lastname;
	
	private Employe employe;

	public IEmployerLocal getEmployerLocal() {
		return employerLocal;
	}

	public void setEmployerLocal(IEmployerLocal employerLocal) {
		this.employerLocal = employerLocal;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	
	public String login(){
		
		String navigateTo=null;
		
		employe = employerLocal.authentification(name, lastname);
		
		if (employe!= null) {
			navigateTo= "/pages/welcome?faces-redirect=true";
		}
		else {
			FacesContext.getCurrentInstance().addMessage("test:btn", new FacesMessage("Bad connexion"));
		}
		
		return navigateTo;
	
	}

	public String logOut(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login?faces-redirect=true";
	}

	
}
