package tn.esprit.jpa;

import javax.persistence.Embeddable;

@Embeddable
public class Adresse{

	private String gouvernorat;
	private String ville;
	private String rue;
	private Integer numMaison;
	public String getGouvernorat() {
		return gouvernorat;
	}
	public void setGouvernorat(String gouvernorat) {
		this.gouvernorat = gouvernorat;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public Integer getNumMaison() {
		return numMaison;
	}
	public void setNumMaison(Integer numMaison) {
		this.numMaison = numMaison;
	}
	
	
}
