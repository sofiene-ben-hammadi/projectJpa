package tn.esprit.jpa;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Technicien extends Employe implements Serializable{

	private String grade;

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}
}
