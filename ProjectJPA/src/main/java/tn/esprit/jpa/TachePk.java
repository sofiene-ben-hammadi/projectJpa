package tn.esprit.jpa;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TachePk implements Serializable {

	private Integer idprojet;
	
	private Integer idEmp;
	
//	private String name

	public Integer getIdprojet() {
		return idprojet;
	}

	public void setIdprojet(Integer idprojet) {
		this.idprojet = idprojet;
	}

	public Integer getIdEmp() {
		return idEmp;
	}

	public void setIdEmp(Integer idEmp) {
		this.idEmp = idEmp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idEmp == null) ? 0 : idEmp.hashCode());
		result = prime * result + ((idprojet == null) ? 0 : idprojet.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TachePk other = (TachePk) obj;
		if (idEmp == null) {
			if (other.idEmp != null)
				return false;
		} else if (!idEmp.equals(other.idEmp))
			return false;
		if (idprojet == null) {
			if (other.idprojet != null)
				return false;
		} else if (!idprojet.equals(other.idprojet))
			return false;
		return true;
	}


	
}
