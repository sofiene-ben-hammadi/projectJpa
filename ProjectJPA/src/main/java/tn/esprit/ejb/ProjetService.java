package tn.esprit.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.esprit.jpa.Projet;

@Stateless
public class ProjetService implements IprojetLocal, IProjetRemote{

	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public void addProjet(Projet p) {
		// TODO Auto-generated method stub
		entityManager.persist(p);
	}

	@Override
	public Projet updateProjet(Projet p) {
		// TODO Auto-generated method stub
		return entityManager.merge(p);
	}

	@Override
	public void deleteProjet(Integer id) {
		// TODO Auto-generated method stub
		entityManager.remove(findById(id));
	}

	@Override
	public Projet findById(Integer id) {
		// TODO Auto-generated method stub
		return entityManager.find(Projet.class, id);
	}

	@Override
	public List<Projet> findAll() {
		// TODO Auto-generated method stub
		
		Query req= entityManager.createQuery("select p from Projet p");
		return req.getResultList();
	}

	
}
