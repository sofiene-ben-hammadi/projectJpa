package tn.esprit.ejb;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.jpa.Projet;

@Local
public interface IprojetLocal {

	public void addProjet(Projet p);
	public Projet updateProjet(Projet p);
	public void deleteProjet(Integer id);
	public Projet findById(Integer id);
	public List<Projet> findAll();
	
}
