package tn.esprit.ejb;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.jpa.Employe;

@Remote
public interface IEmployerRemote {

	public void addEmp(Employe e);
	public Employe updateEmp(Employe e);
	public void deleteEmp(Integer id);
	public Employe findById(Integer id);
	public List<Employe> findAllEmploye();
	
}
