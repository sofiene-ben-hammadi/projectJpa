package tn.esprit.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.jpa.Employe;

@Stateless
public class EmployeService implements IEmployerLocal, IEmployerRemote {

	@PersistenceContext
	EntityManager em;
	
	
	@Override
	public void addEmp(Employe e) {
		// TODO Auto-generated method stub
		em.persist(e);
	}

	@Override
	public Employe updateEmp(Employe e) {
		// TODO Auto-generated method stub
		return em.merge(e);
	}

	@Override
	public void deleteEmp(Integer id) {
		// TODO Auto-generated method stub
		em.remove(findById(id));
	}

	@Override
	public Employe findById(Integer id) {
		// TODO Auto-generated method stub
		return em.find(Employe.class, id);
	}

	@Override
	public List<Employe> findAllEmploye() {
		// TODO Auto-generated method stub
		TypedQuery<Employe> req = em.createQuery("select e from Employe e", Employe.class);
//		Query req1 = em.createNamedQuery("select e from Employe e");
		return req.getResultList();
	}

	@Override
	public Employe authentification(String name, String lastname) {
		// TODO Auto-generated method stub
		TypedQuery<Employe> req = em.createQuery("select e from Employe e where name like "
				+ ":nom and lastname like :prenom",Employe.class);
		req.setParameter("nom", name).setParameter("prenom", lastname);
		try {
			return req.getSingleResult();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("user not found");
		}
		return null;
	}

	
}
