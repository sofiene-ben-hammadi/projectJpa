package tn.esprit.ejb;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.jpa.Projet;

@Remote
public interface IProjetRemote {

	public void addProjet(Projet p);
	public Projet updateProjet(Projet p);
	public void deleteProjet(Integer id);
	public Projet findById(Integer id);
	public List<Projet> findAll();
}
