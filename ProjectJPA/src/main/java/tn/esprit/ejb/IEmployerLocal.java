package tn.esprit.ejb;

import java.util.List;

import javax.ejb.Local;

import tn.esprit.jpa.Employe;

@Local
public interface IEmployerLocal {

	public void addEmp(Employe e);
	public Employe updateEmp(Employe e);
	public void deleteEmp(Integer id);
	public Employe findById(Integer id);
	public List<Employe> findAllEmploye();
	public Employe authentification(String name, String lastname);
}
